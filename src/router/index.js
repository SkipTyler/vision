import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
import Main from "../components/main"
import sdfs from "../components/solution"

const router = new VueRouter({
    mode: 'history',
    hash: false,
    routes: [
        { path: '/', component: Main },
        { path: '/test', component: sdfs },
        {
            path: "*",
            redirect: "/"
        }
    ]
});

export default router;