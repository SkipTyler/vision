import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		succShow: false,
		pageShow: 'main',

	},
	actions: {
		filledInput() {
			const target = event.target;
			const parent = target.parentNode;
			const span = parent.querySelector('span');
			if (target.value.length > 0) {
				span.classList.add('active');
			} else {
				span.classList.remove('active');
			}
		},

		sendForm({commit}, param) {
			const target = event.target;

			const crossDomainPost = (q) => {
				const chatBot = 383655115;
				// const chatId = 380460973;
				const chatId = 97274125;
				const iframe = document.createElement("iframe");
				const uniqueString = "CHANGE_THIS_TO_SOME_UNIQUE_STRING";
				document.body.appendChild(iframe);
				iframe.style.display = "none";
				iframe.contentWindow.name = uniqueString;

				const form = document.createElement("form");
				form.target = uniqueString;

				if (q === 'feedback') {
					const subject = 'Feedback';
					const field1 = 'Name:';
					const field2 = 'Company:';
					const field3 = 'Email:';
					const field4 = 'Phone:';

					form.action =
						"https://api.telegram.org/bot" + chatBot + ":AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=" + chatId + "&text=" + subject + "%0A" + field1 + " " + document.querySelector('input[name="feedName"]').value + "%0A" + field2 + " " + document.querySelector('input[name="feedCompany"]').value  + "%0A" + field3 + " " + document.querySelector('input[name="feedEmail"]').value  + "%0A" + field4 + " " + document.querySelector('input[name="feedPhone"]').value;
				} else if (q === 'find') {
					const subject = 'Find ambassador';
					const field1 = 'Company:';
					const field2 = 'City:';
					const field3 = 'Phone:';
					const field4 = 'Email:';
					const field5 = 'Info:';

					form.action =
						"https://api.telegram.org/bot" + chatBot + ":AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=" + chatId + "&text=" + subject + "%0A" + field1 + " " + document.querySelector('input[name="FindCompany"]').value + "%0A" + field2 + " " + document.querySelector('input[name="FindCity"]').value  + "%0A" + field3 + " " + document.querySelector('input[name="FindTel"]').value  + "%0A" + field4 + " " + document.querySelector('input[name="FindEmail"]').value  + "%0A" + field5 + " " + document.querySelector('textarea[name="FindInfo"]').value;
				} else if (q === 'become') {
					const subject = 'Become ambassador';
					const field2 = 'City:';
					const field3 = 'Phone:';
					const field4 = 'Email:';
					const field5 = 'Social media:';
					const field6 = 'Story:';

					form.action =
						"https://api.telegram.org/bot" + chatBot + ":AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=" + chatId + "&text=" + subject + "%0A" + field2 + " " + document.querySelector('input[name="BecomeCity"]').value  + "%0A" + field3 + " " + document.querySelector('input[name="BecomeTel"]').value  + "%0A" + field4 + " " + document.querySelector('input[name="BecomeEmail"]').value  + "%0A" + field5 + " " + document.querySelector('input[name="BecomeMedia"]').value  + "%0A" + field6 + " " + document.querySelector('textarea[name="BecomeStory"]').value;
				} else if (q === 'founder') {
					const subject = 'Founder question';
					const field1 = 'Name:';
					const field3 = 'Email:';
					const field4 = 'Phone:';

					form.action =
						"https://api.telegram.org/bot" + chatBot + ":AAFbKchTA9zDxOSfRW-8IVH2A6r3VTAjUV4/sendMessage?chat_id=" + chatId + "&text=" + subject + "%0A" + field1 + " " + document.querySelector('input[name="founderName"]').value + "%0A" +  field3 + " " + document.querySelector('input[name="founderEmail"]').value  + "%0A" + field4 + " " + document.querySelector('input[name="founderPhone"]').value;

				}

				form.method = "POST";

				// repeat for each parameter
				const input = document.createElement("input");
				input.type = "hidden";
				input.name = "INSERT_YOUR_PARAMETER_NAME_HERE";
				input.value = "INSERT_YOUR_PARAMETER_VALUE_HERE";
				form.appendChild(input);

				document.body.appendChild(form);
				form.submit();
			}

			crossDomainPost(param);

			target.reset();

			Array.from(target.querySelectorAll('span')).forEach(el => {
				el.classList.remove('active');
			});
			target.classList.add('hide');
			this.state.succShow = true;
		},

		hideSucces({commit}) {
			this.state.succShow = false;
			Array.from(document.querySelectorAll('form')).forEach(el => {
				el.classList.remove('hide');
			});
			

			if (this.state.pageShow != 'main') {
				commit('showPage', 'main')
			}
		},
	},
	mutations: {
		showPage(state, param) {
			state.pageShow = param;
		}
	}
})
